if __name__ == '__main__':
    addresses = [
        {
            "city": "Warsaw",
            "street": "Poznańska",
            "house_no": "25",
            "post_code": "02-235"
        },
        {
            "city": "Paris",
            "street": "LaDefanse",
            "house_no": "10",
            "post_code": "152844"
        },
        {
            "city": "Berlin",
            "street": "KarlsPlazze",
            "house_no": "18",
            "post_code": "11-12536"
        }
    ]
    print(addresses[-1]["post_code"])
    print(addresses[1]["city"])
    addresses[0]['street'] = 'Krótka'
    print(addresses)
