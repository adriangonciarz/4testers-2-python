import random


def generate_animal_dictionary(is_mammal):
    if is_mammal:
        animal_type = 'mammal'
        animal_kind = random.choice(['dog', 'cat', 'zebra'])
    else:
        animal_type = 'bird'
        animal_kind = random.choice(['parrot', 'eagle', 'hawk', 'sparrow'])
    animal_age = random.randint(1, 3)
    return {
        'kind': animal_kind,
        'type': animal_type,
        'age': animal_age
    }


def generate_list_of_animals_evenly_distributed(number_of_pairs):
    list_of_animals = []
    for i in range(number_of_pairs):
        list_of_animals.append(generate_animal_dictionary(True))
        list_of_animals.append(generate_animal_dictionary(False))
    return list_of_animals


def print_description_for_every_animal_in_the_list(list_of_animals):
    for animal in list_of_animals:
        print(f'This animal is of kind {animal["kind"]} and is of age {animal["age"]}')


# Generate 10 dictionaries, half of them mammals, half birds
if __name__ == '__main__':
    animals_list = generate_list_of_animals_evenly_distributed(5)
    print_description_for_every_animal_in_the_list(animals_list)
