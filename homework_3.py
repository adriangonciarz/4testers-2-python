def convert_celsius_to_fahrenheit(temperatures_in_celsius):
    temps_fahrenheit = []
    for temperature in temperatures_in_celsius:
        temps_fahrenheit.append(round(temperature * 9 / 5 + 32, 2))
    return temps_fahrenheit


if __name__ == '__main__':
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print(convert_celsius_to_fahrenheit(temps_celsius))
    convert_celsius_to_fahrenheit(100)
