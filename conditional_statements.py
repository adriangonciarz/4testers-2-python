def print_word_length_description(name):
    if len(name) > 5:
        print(f'The name {name} is longer than 5 characters')
    else:
        print(f'The name {name} is 5 characters or shorter')


def check_if_normal_conditions(celsius, pressure):
    if (celsius == 0) & (pressure == 1013):
        return True
    else:
        return False


def is_it_4testers_day(day_of_week):
    if day_of_week == 'monday' or day_of_week == 'wednesday' or day_of_week == 'friday':
        return True
    else:
        return False


def is_it_japanese_car_brand(car_brand):
    if car_brand in ['Toyota', 'Suzuki', 'Mazda']:
        return True
    else:
        return False


def get_grade_for_exam_points(number_of_points):
    if number_of_points >= 90:
        return 5
    elif number_of_points >= 75:
        return 4
    elif number_of_points >= 50:
        return 3
    else:
        return 2


def get_country_for_car_brand(car_brand):
    if car_brand in ['Toyota', 'Suzuki', 'Mazda']:
        return 'Japan'
    elif car_brand in ['BMW', 'Mercedes', 'Opel', 'Volkswagen', 'Audi']:
        return 'Germany'
    elif car_brand in ['Fiat', 'Ferrari', 'Lamborghini']:
        return 'Italy'
    else:
        return 'Unknown'


if __name__ == '__main__':
    long_name = 'Adrianna'
    short_name = 'Adam'

    print_word_length_description(long_name)
    print_word_length_description(short_name)

    print(check_if_normal_conditions(0, 1013))
    print(check_if_normal_conditions(1, 1014))
    print(check_if_normal_conditions(0, 1014))
    print(check_if_normal_conditions(1, 1014))

    print(is_it_4testers_day('monday'))
    print(is_it_4testers_day('thursday'))

    print(is_it_japanese_car_brand('BMW'))
    print(is_it_japanese_car_brand('KIA'))
    print(is_it_japanese_car_brand('Toyota'))

    print(get_grade_for_exam_points(91))
    print(get_grade_for_exam_points(90))
    print(get_grade_for_exam_points(89))
    print(get_grade_for_exam_points(76))
    print(get_grade_for_exam_points(75))
    print(get_grade_for_exam_points(74))
    print(get_grade_for_exam_points(50))
    print(get_grade_for_exam_points(49))

    print(get_country_for_car_brand('BMW'))
    print(get_country_for_car_brand('Ferrari'))
    print(get_country_for_car_brand('KIA'))
