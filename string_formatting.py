my_name = "Adrian"
favourite_movie = "Star Wars"
favourite_actor = "Clint Eastwood"

print("My name is ", my_name, ". My favourite movie is ", favourite_movie, ". My favourite actor is ", favourite_actor,
      ".", sep="")

bio = f"My name is {my_name}. My favourite movie is {favourite_movie}. My favourite actor is {favourite_actor}."
print(bio)

calculation_example = f"Sum of 2 and 4 is {2 + 4}."
print(calculation_example)


# Exercise 1 - Print welcome message to a person in a city
def print_hello_message(name, city):
    name_capitalized = name.capitalize()
    city_capitalized = city.capitalize()
    # print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")
    print(f"Witaj {name_capitalized}! Miło Cię widzieć w naszym mieście: {city_capitalized}!")


print_hello_message("Michał", "Toruń")
print_hello_message("Beata", "Gdynia")
print_hello_message("adam", "kraków")


# Exercise 2 - Generate email in 4testers.pl domain using first and last name
def get_email(name, surname):
    return f'{name.lower()}.{surname.lower()}@4testers.pl'


print(get_email("Janusz", "Nowak"))
print(get_email("Barbara", "Kowalska"))

# Escaping characters
print('McDonald\'s')
print("My favourite book is \"Steppenwolf\"")
print('My favourite book is "Steppenwolf"')

